## GitLab UX Research
Contains all research undertaken by GitLab's UX researcher.

## Contributing

GitLab is an open source project and we are very happy to accept community
contributions. Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

## Research archive
### Dashboards

2017-08 [Dashboards (Workflow User Interviews)](#24)

### Issues

2018-01 [Issue display & sort order](#39)

2017-02 [Issues Filter](#1)

2017-02 [Exploratory interviews regarding project management / issues.](#2)

2017-08 [Issues (Workflow User Interviews)](#27)

2017-11 [Distinguishing between an issue and merge request](#32)

2017-11 [Moving an issue between projects](#32)

### Merge requests

2017-07 [Merge request page](#21)

2017-08 [Merge Requests (Workflow User Interviews)](#29)

2017-11 [Distinguishing between an issue and merge request](#32)

### Navigation

2017-03 [1.1 General navigation tasks](#3)

2017-04 [1.3 Global vs contextual navigation](#7)

2017-04 [1.2 General navigation tasks - follow up testing](#5)

2017-11 [Changing the sidebar colour](#32)

### Notifications (Todos, emails, realtime alerts, etc)

2017-08 [Notifications (Workflow User Interviews)](#25)

### Onboarding

2017-08 [Challenges with onboarding new users (Workflow User Interviews)](#28)

2017-11 [User feedback received via email (D.P)](#34)

### Projects

2017-08 [Projects (Workflow User Interviews)](#26)

2017-11 [Creating a new project](#32)

### Wiki

2017-08 [Wiki (Workflow User Interviews)](#23)


## Personas
https://docs.gitlab.com/ce/development/ux_guide/users.html

## Do you want to shape the future of GitLab?
We want your feedback on how we can continue to improve GitLab. By joining our research panel, you’ll be the first to see new features and your thoughts will drive product improvements for a better user experience.

[Join GitLab's research panel](https://about.gitlab.com/researchpanel/)


## Project members

Owner: @sarahod

Maintainers:
- @katokpara
- @cperessini
- @dimitrieh
- @hazelyang
- @pedroms
- @sarrahvesselov
- @tauriedavis
 
Please note this project is used for issue tracking only.